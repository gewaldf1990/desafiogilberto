package controller;

import javax.swing.JOptionPane;
import javax.swing.JRootPane;

import model.Dish;
import model.DishList;
import service.ServiceClass;

public class PrincipalController {

	private final static Dish tipoUm = new Dish("Lasanha", "");
	private final static Dish tipoDois = new Dish("Bolo de Chocolate", "");

	private static final DishList pratosTipoUm = new DishList();
	private static final DishList pratosTipoDois = new DishList();

	static JRootPane rootPane = new JRootPane();

	private static int resposta;

	public static void parametrosIniciais() {

	}

	public static void inicializa() {

		ServiceClass servico = new ServiceClass();

		pratosTipoUm.getlAlimentos().add(tipoUm);
		pratosTipoDois.getlAlimentos().add(tipoDois);
		while (true) {

			JRootPane rootPane = new JRootPane();

			resposta = JOptionPane.showConfirmDialog(rootPane, "Pense em um prato que gosta", "Confirm",
					JOptionPane.YES_NO_OPTION);

			if(resposta == JOptionPane.NO_OPTION) {
				return;
			}
			
			resposta = JOptionPane.showConfirmDialog(rootPane, "O prato que voc� pensou � Massa ?", "Confirm",
					JOptionPane.YES_NO_OPTION);

			if (resposta == JOptionPane.YES_OPTION) {
				resposta = servico.MakeGuessDish(pratosTipoUm, resposta);
				continue;
			}

			resposta = servico.MakeGuessDish(pratosTipoDois, resposta);

		}
	}

	public static void main(String[] args) {

		inicializa();

	}

}

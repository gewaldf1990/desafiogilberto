package service;

import javax.swing.JOptionPane;
import javax.swing.JRootPane;

import model.Dish;
import model.DishList;

public class ServiceClass {
	public static JRootPane rootPane = new JRootPane();

	private static void ok() {

		JOptionPane.showMessageDialog(rootPane, "ok de novo!", "ok", JOptionPane.INFORMATION_MESSAGE);
	}

	public int MakeGuessDish(DishList lAlimentos, int resposta) {
		int contador;
		int tamanhoList = lAlimentos.getlAlimentos().size() - 1;

		
		
		for (contador = tamanhoList; contador > 0; contador--) {
			resposta = perguntaPrato(lAlimentos, contador, true);

			if (resposta == JOptionPane.YES_OPTION) {

				resposta = perguntaPrato(lAlimentos, contador, false);

				if (resposta == JOptionPane.YES_OPTION) {

					ok();
					break;
				} else if ((resposta == JOptionPane.NO_OPTION) && (contador == 0)) {

					addDish(lAlimentos, contador);
					break;
				}
			}
		}

		if (contador == 0) {

			resposta = perguntaPrato(lAlimentos, contador, false);

			if (resposta == JOptionPane.YES_OPTION) {

				ok();
				return -1;

			}

			addDish(lAlimentos, contador);
		}
		return -1;
	}

	private static void addDish(DishList lAlimento, int ordemPrato) {
		lAlimento.getlAlimentos().add(montaObjetoPratoNovo(lAlimento, ordemPrato));
	}

	private static int perguntaPrato(DishList lAlimentos, int contador, boolean caracteristica) {
		if (caracteristica) {

			return JOptionPane.showConfirmDialog(rootPane,
					"O prato que pensou � ".concat(lAlimentos.getlAlimentos().get(contador).getTipo()).concat(" ?"),
					"Confirm", JOptionPane.YES_NO_OPTION);
		}

		return JOptionPane.showConfirmDialog(rootPane,
				"O prato que pensou � ".concat(lAlimentos.getlAlimentos().get(contador).getNome()).concat(" ?"),
				"Confirm", JOptionPane.YES_NO_OPTION);
	}

	private static Dish montaObjetoPratoNovo(DishList lAlimento, int ordemPrato) {
		String descricaoPrato = JOptionPane.showInputDialog(rootPane, "Qual prato voc� pensou ?", "Desisto",
				JOptionPane.QUESTION_MESSAGE);
		String tipo = JOptionPane.showInputDialog(
				rootPane, descricaoPrato.concat(" � ________ mas ")
						.concat(lAlimento.getlAlimentos().get(ordemPrato).getNome()).concat(" n�o."),
				"Complete", JOptionPane.QUESTION_MESSAGE);

		Dish prato = new Dish(descricaoPrato, tipo);

		return prato;
	}

}
